package com.example.product.retroft


import com.example.product.productAdd.model.ProductAddResponse
import com.example.product.productList.model.ProductListResponse
import retrofit2.http.*

interface ApiInterface {
    // API for device key for registration

    @GET("get")
    @Headers("Content-type:application/json")
    suspend fun productList(): ArrayList<ProductListResponse>

    @FormUrlEncoded
    @POST("add")
    suspend fun addProductList(
        @Field("name") name:String,
        @Field("product_name") productName:String,
        @Field("product_type") productType:String,
        @Field("price") price:String,
        @Field("tax") tax:String,
    ): ProductAddResponse


}
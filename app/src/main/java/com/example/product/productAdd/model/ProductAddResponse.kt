package com.example.product.productAdd.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import okhttp3.MultipartBody

data class ProductAddResponse(
    @SerializedName("message")
    @Expose
    var message: String,
    @SerializedName("success")
    @Expose
    var success: Boolean,
    @SerializedName("product_id")
    @Expose
    var productId: String,
)

{
    override fun toString(): String {
        return "ProductAddResponse(message=$message,success=$success,productId=$productId)"
    }
}
package com.example.product.productAdd.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.product.productAdd.model.ProductAddRequest
import com.example.product.productAdd.model.ProductAddResponse
import com.example.product.productList.model.ProductListResponse
import com.example.product.retroft.MyRetrofitBuilder
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException

object ProductAddRepository  {
    private const val serverError =
        "{\"message\":\"Some Thing Went Wrong\",\"success\":false}"
    var job: CompletableJob? = null


    fun addProductList(productAddRequest: ProductAddRequest): LiveData<ProductAddResponse> {
        job = Job()
        return object : LiveData<ProductAddResponse>() {
            override fun onActive() {
                super.onActive()
                job?.let { theJob ->
                    CoroutineScope(Dispatchers.IO + theJob).launch {
                        try {
                            val user = MyRetrofitBuilder.apiService.addProductList(productAddRequest.name!!,productAddRequest.product_name!!,productAddRequest.product_type!!,productAddRequest.price!!,productAddRequest.tax!!)
                            Log.e("", "user$user")
                            withContext(Dispatchers.Main) {
                                value = user
                                theJob.complete()
                            }
                        } catch (ex: HttpException) {
                            var jObjError = ""
                            jObjError = try {
                                if (ex.code() == 500) {
                                    serverError
                                } else {
                                    JSONObject(ex.response()!!.errorBody()!!.string()).toString()
                                }
                            }catch (e: JSONException){
                                serverError
                            }
                            val data: String = jObjError
                            val gson = GsonBuilder().create()
                            val testModel = gson.fromJson(data, ProductAddResponse::class.java)
                            Log.e("", "testModel$testModel")
                            withContext(Dispatchers.Main) {
                                value = testModel
                                theJob.complete()
                            }
                        }catch (e: IOException) {
                            var jObjError = ""
                            jObjError = serverError
                            val data: String = jObjError
                            val gson = GsonBuilder().create()
                            val testModel = gson.fromJson(data, ProductAddResponse::class.java)
                            Log.e("", "testModel$testModel")
                            withContext(Dispatchers.Main) {
                                value = testModel
                                theJob.complete()
                            }
                            Log.e("", "IOException ${e.message}")
                        } catch (e: Exception) {
                            Log.e("", "user${e.message}")
                        }
                    }
                }

            }
        }
    }


    fun cancelJobs() {
        job?.cancel()
    }
}
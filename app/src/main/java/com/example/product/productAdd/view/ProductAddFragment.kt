package com.example.product.productAdd.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.product.launchActivity.MainActivity
import com.example.product.R
import com.example.product.databinding.FragmentProductAddBinding
import com.example.product.productAdd.model.ProductAddRequest
import com.example.product.productAdd.viewModel.ProductAddViewModel
import com.example.product.utility.DialogUtils

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProductAddFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProductAddFragment : Fragment() {
    private val args: ProductAddFragmentArgs by navArgs()
   // private val args: Product by navArgs()
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? MainActivity)?.updateToolBar("Add Product", true)
        setHasOptionsMenu(false)
      /*  tvTitle = view.findViewById(R.id.tvTitle)
        args.title?.let {
            tvTitle.text = it
        }*/
    }

    private lateinit var binding: FragmentProductAddBinding
    private lateinit var productAddViewModel:ProductAddViewModel
    private var progressBar = DialogUtils
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
       // return inflater.inflate(R.layout.fragment_product_add, container, false)
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_product_add, container, false)
        productAddViewModel = ViewModelProvider(this)[ProductAddViewModel::class.java]
        binding.productAddViewModel = productAddViewModel
        binding.lifecycleOwner = this
        productAddViewModel.getProductDetails().observe(viewLifecycleOwner){
            if(it.name == null || it.name.isEmpty()){
                productAddViewModel.nameError.value = "Please enter name"
                return@observe
            }else{
                binding.tlName.isErrorEnabled = false
            }

            if(it.product_name == null || it.product_name.isEmpty()){
                productAddViewModel.productNameError.value = "Please enter product Name"
                return@observe
            }else{
                binding.tlProductName.isErrorEnabled = false
            }

            if(it.product_type == null || it.product_type.isEmpty()){
                productAddViewModel.productTypeError.value = "Please enter product type"
                return@observe
            }else{
                binding.tlProductType.isErrorEnabled = false
            }

            if(it.price == null || it.price.isEmpty()){
                productAddViewModel.priceError.value = "Please enter price"
                return@observe
            }else{
                binding.tlProductPrice.isErrorEnabled = false
            }

            if(it.tax == null || it.tax.isEmpty()){
                productAddViewModel.taxError.value = "Please enter tax"
                return@observe
            }else{
                binding.tlProductTax.isErrorEnabled = false
            }


            val productAddRequest = ProductAddRequest(it.name,it.product_name,it.product_type,it.price,it.tax)

            productAddViewModel.setProductAddRequest(productAddRequest)

            progressBar.show(context!!)

        }

        productAddViewModel.productAddResponse.observe(viewLifecycleOwner){ it ->
            Log.e("ProductList","$it")
            progressBar.dialog?.dismiss()
            DialogUtils.showAlertPopup(it.message,context!!)
        }

        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProductAddFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProductAddFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
package com.example.product.productAdd.viewModel

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.product.productAdd.model.ProductAddRequest
import com.example.product.productAdd.model.ProductAddResponse
import com.example.product.productAdd.repository.ProductAddRepository
import com.example.product.productList.repository.ProductListRepository

class ProductAddViewModel: ViewModel() {

    var name = MutableLiveData<String>()
    var nameError = MutableLiveData<String>()
    var productName = MutableLiveData<String>()
    var productNameError = MutableLiveData<String>()
    var productType = MutableLiveData<String>()
    var productTypeError = MutableLiveData<String>()
    var price = MutableLiveData<String>()
    var priceError = MutableLiveData<String>()
    var tax = MutableLiveData<String>()
    var taxError = MutableLiveData<String>()

    private var productAddRequestLiveData = MutableLiveData<ProductAddRequest>()
    fun getProductDetails(): MutableLiveData<ProductAddRequest> {
        return productAddRequestLiveData
    }

    fun onClick(view: View?) {
        val loginUser = ProductAddRequest(name.value,productName.value,productType.value,price.value,tax.value)
        productAddRequestLiveData.value = loginUser
    }





    private val productAddRequest: MutableLiveData<ProductAddRequest> = MutableLiveData()

    val productAddResponse: LiveData<ProductAddResponse> =
        Transformations.switchMap(productAddRequest){
            ProductAddRepository.addProductList(it)
        }

    fun setProductAddRequest(productAddRequestValue: ProductAddRequest){
        productAddRequest.value = productAddRequestValue
    }
}
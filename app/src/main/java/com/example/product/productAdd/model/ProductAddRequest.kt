package com.example.product.productAdd.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ProductAddRequest(
    @SerializedName("name")
    @Expose
    val name: String?,
    @SerializedName("product_name")
    @Expose
    val product_name: String?,
    @SerializedName("product_type")
    @Expose
    val product_type: String?,
    @SerializedName("price")
    @Expose
    val price: String?,
    @SerializedName("tax")
    @Expose
    val tax: String?,
)
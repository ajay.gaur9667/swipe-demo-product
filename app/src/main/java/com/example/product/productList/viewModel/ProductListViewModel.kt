package com.example.product.productList.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.product.productAdd.model.ProductAddRequest
import com.example.product.productAdd.model.ProductAddResponse
import com.example.product.productList.model.ProductListResponse
import com.example.product.productList.repository.ProductListRepository

class ProductListViewModel: ViewModel() {
    private val productList: MutableLiveData<String> = MutableLiveData()


    val getProductListResponse: LiveData<ArrayList<ProductListResponse>> =
        Transformations.switchMap(productList){
            ProductListRepository.getProductList(it)
        }

    fun setProductListRequest(productListValue: String){
        productList.value = productListValue
    }


}
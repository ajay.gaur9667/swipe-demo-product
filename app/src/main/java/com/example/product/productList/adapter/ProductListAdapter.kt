package com.example.product.productList.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.product.R
import com.example.product.productList.model.ProductListResponse

class ProductListAdapter(private val productListResponse: ArrayList<ProductListResponse>) : RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.image_view_movie_image)
        val textView: TextView = itemView.findViewById(R.id.text_view_name)
        val text_view_date: TextView = itemView.findViewById(R.id.text_view_date)
        val text_view_rating: TextView = itemView.findViewById(R.id.text_view_rating)
        val text_view_tax: TextView = itemView.findViewById(R.id.text_view_tax)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_product_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemsViewModel = productListResponse[position]
        Glide.with(holder.imageView.context)
            .load(itemsViewModel.image)
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(holder.imageView)

        // sets the text to the textview from our itemHolder class
        holder.textView.text = itemsViewModel.productName
        holder.text_view_date.text = itemsViewModel.productType
        holder.text_view_rating.text = itemsViewModel.price
        holder.text_view_tax.text = itemsViewModel.tax
    }

    override fun getItemCount(): Int {
        return  productListResponse.size
    }
}
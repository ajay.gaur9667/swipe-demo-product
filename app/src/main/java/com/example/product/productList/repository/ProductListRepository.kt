package com.example.product.productList.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.product.productList.model.ProductListResponse
import com.example.product.retroft.MyRetrofitBuilder
import com.google.gson.GsonBuilder
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException

object ProductListRepository  {
    private const val serverError =
        "{\"message\":\"Some Thing Went Wrong\",\"success\":false}"
    var job: CompletableJob? = null


    fun getProductList(productList: String): LiveData<ArrayList<ProductListResponse>> {
        job = Job()
        return object : LiveData<ArrayList<ProductListResponse>>() {
            override fun onActive() {
                super.onActive()
                job?.let { theJob ->
                    CoroutineScope(Dispatchers.IO + theJob).launch {
                        try {
                            val user = MyRetrofitBuilder.apiService.productList()
                            Log.e("", "user$user")
                            withContext(Dispatchers.Main) {
                                value = user
                                theJob.complete()
                            }
                        } catch (ex: HttpException) {
                            var jObjError = ""
                            jObjError = try {
                                if (ex.code() == 500) {
                                    serverError
                                } else {
                                    JSONObject(ex.response()!!.errorBody()!!.string()).toString()
                                }
                            }catch (e: JSONException){
                                serverError
                            }
                            val data: String = jObjError
                            val gson = GsonBuilder().create()
                            val testModel = gson.fromJson(data, ProductListResponse::class.java)
                            Log.e("", "testModel$testModel")
                            withContext(Dispatchers.Main) {
                                //value = testModel
                                theJob.complete()
                            }
                        }catch (e: IOException) {
                            var jObjError = ""
                            jObjError = serverError
                            val data: String = jObjError
                            val gson = GsonBuilder().create()
                            val testModel = gson.fromJson(data, ProductListResponse::class.java)
                            Log.e("", "testModel$testModel")
                            withContext(Dispatchers.Main) {
                              //  value = testModel
                                theJob.complete()
                            }
                            Log.e("", "IOException ${e.message}")
                        } catch (e: Exception) {
                            Log.e("", "user${e.message}")
                        }
                    }
                }

            }
        }
    }

    fun cancelJobs() {
        job?.cancel()
    }
}
package com.example.product.productList.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ProductListResponse(
    @SerializedName("image")
    @Expose
    var image: String,
    @SerializedName("price")
    @Expose
    var price: String,
    @SerializedName("product_name")
    @Expose
    var productName: String,
    @SerializedName("product_type")
    @Expose
    var productType: String,
    @SerializedName("tax")
    @Expose
    var tax: String,

)

{
    override fun toString(): String {
        return "ProductListResponse(image=$image,price$price,productName=$productName,productType=$productType,tax=$tax)"
    }
}
package com.example.product.productList.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.product.launchActivity.MainActivity
import com.example.product.R
import com.example.product.productList.adapter.ProductListAdapter
import com.example.product.productList.model.ProductListResponse
import com.example.product.productList.viewModel.ProductListViewModel
import com.example.product.utility.DialogUtils
import com.google.android.material.floatingactionbutton.FloatingActionButton


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProductListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProductListFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private  val arrayListProductListResponse:ArrayList<ProductListResponse> = ArrayList()
    private lateinit var productListAdapter: ProductListAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var normalFAB: FloatingActionButton
    private lateinit var productListViewModel: ProductListViewModel
    private var progressBar = DialogUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? MainActivity)?.updateToolBar("Product Details")
        setHasOptionsMenu(true)
        normalFAB = view.findViewById(R.id.normalFAB)
        normalFAB.setOnClickListener {
            navigateToDetails()
        }
        recyclerView = view.findViewById(R.id.recyclerView)
        productListAdapter = ProductListAdapter(arrayListProductListResponse)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = productListAdapter
        productListViewModel = ViewModelProvider(this)[ProductListViewModel::class.java]
        productListViewModel.setProductListRequest("1")
        progressBar.show(context!!)
        productListViewModel.getProductListResponse.observe(viewLifecycleOwner){
            Log.e("ProductList","$it")
            for (item in it.listIterator()){
                arrayListProductListResponse.add(item)
            }
            productListAdapter.notifyDataSetChanged()
            progressBar.dialog?.dismiss()

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    private fun navigateToDetails() {
        findNavController().navigate(
            ProductListFragmentDirections.actionProductListFragmentToProductAddFragment(
                "Here is the details"
            )
        )
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProductListFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProductListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
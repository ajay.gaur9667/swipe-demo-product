package com.example.product.utility

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import com.example.product.R


object DialogUtils {
    var dialog: Dialog? = null

    /*This method used to show the progress bar*/
    fun show(context: Context): Dialog {
        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layoutInflater.inflate(R.layout.progress_dialog, null)
        dialog = Dialog(context, R.style.CustomProgressBarTheme)
        if (dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawableResource(R.color.bg_color)
        }
        dialog!!.setContentView(view)
        dialog!!.show()
        dialog!!.setCancelable(false)
        return dialog!!
    }

    /*This Method used to display the Alert Popup*/
    fun showAlertPopup(alert: String, context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(alert).setCancelable(true).setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
        }
        builder.create().show()
    }



}